<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jcap.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/md5.js"></script>
<div class="row">
   	    <!--顯示系統APP名稱及Login翻譯的模組，目前用不上
        <?php echo $this->pageTitle=Yii::app()->name . ' - '.UserModule::t("Login"); ?> 
    	-->

        <?php if(Yii::app()->user->hasFlash('loginMessage')): ?>

        <div class="success">
        <?php echo Yii::app()->user->getFlash('loginMessage'); ?>
        </div>

        <?php endif; ?>
        <!--
    	<p><?php echo UserModule::t("Get started"); ?></p>
        -->
     <div  class="small-6 small-centered columns">
        <h3 class="subheader" align="right"><?php echo Yii::app()->getModule('oxexam')->loadconf('slogan'); ?></h3>
        <p></p>
        <!-- form begin -->
    	<div class="form"  onsubmit="return jcap();">
        
            <?php echo CHtml::beginForm(); ?>
            <?php echo CHtml::errorSummary($model); ?>
            <fieldset><legend><?php echo UserModule::t("Get started"); ?></legend>
                <?php echo CHtml::activeLabelEx($model,'username'); ?>
                <?php echo CHtml::activeTextField($model,'username') ?>
                <?php echo CHtml::activeLabelEx($model,'password'); ?>
                <?php echo CHtml::activePasswordField($model,'password') ?>
                 <!--輸入驗證碼欄位-->
                <?php echo CHtml::activeLabelEx($model,UserModule::t("Verify Code")); ?>
                <?php echo CHtml::activeTextField($model,'verifyCode'); ?>
        
            <!--下次自動登入功能
            <div class="row rememberMe">
                <?php echo CHtml::activeCheckBox($model,'rememberMe'); ?>
                <?php echo CHtml::activeLabelEx($model,'rememberMe'); ?>
            </div>
            -->
            <!--產生隨機驗證碼功能-->
              <div>
                <?php if(CCaptcha::checkRequirements()): ?> <!-- check GD lib 是否安裝 -->
       	            <?php 
                        $this->widget ( 'CCaptcha', array (
                        'showRefreshButton' => true,
                        'clickableImage' => true,
                        'buttonType' => 'link', 
                        'buttonLabel' => UserModule::t("Change Picture"),
                        'buttonOptions' => array( "class"=>"refreshLabel","style"=>"margin:5px 5px 0" ),
                        'imageOptions' => array ( 'style' => 'vertical-align:middle;cursor:pointer;border:1px #DEDEDE solid;height:60px;float:middle' ) ) );
                    ?>
                <?php CHtml::errorSummary($model,'verifyCode'); ?>
                </div>
            </fieldset>
            <?php endif; ?>
                    
            <div class="row submit">
                <input class="button radius" type="submit" value="<?php echo UserModule::t("Login")?>">
                <img src="<?php echo Yii::app()->getModule('oxexam')->loadconf('systemlogo'); ?>" alt='test' width="167" height="56" align="right">
	        </div>
	        <?php echo CHtml::endForm(); ?>
	    </div>
         <!-- form end -->

        <?php
            $form = new CForm(array(
                    'elements'=>array(
                        'username'=>array(
                        'type'=>'text',
                        'maxlength'=>32,
                        ),
                        'password'=>array(
                        'type'=>'password',
                        'maxlength'=>32,
                        ),
                        'rememberMe'=>array(
                        'type'=>'checkbox',
                        ),
                        'verifyCode'=>array(
                        'type'=>'text',
                        'maxlength'=>6,
                        )
                ),

                    'buttons'=>array(
                        'login'=>array(
                        'type'=>'submit',
                        'label'=>'Login',
                        ),
                ),
            ), $model);
        ?>

    </div>
</div>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    
<!--
    <script src='//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js'></script>
    <script src='http://cdn.foundation5.zurb.com/foundation.js'></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/angular.min-1.2.9.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/angular-sanitize-1.2.0-rc.2.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/test.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/mm-foundation-tpls-0.1.0.min.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/foundation/foundation.js"></script>
-->
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/vendor/jquery.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/foundation.min.js"></script>

</head>
<body>

<div class="container" id="page">
<br /><br />

<?php 
    $user = Yii::app()->getModule('user')->user(Yii::app()->user->id);
?>

<?php
    $mysql_exam_id=$_POST["mysql_exam_id"];
    //Yii::import('application.controllers.ResultController');  // modify for render usersmodel
    $papersmodel=$this->PaperloadModel($mysql_exam_id);
    //$usersmodel=$this->UsersloadModel($user->mysql_u_id); // modify for render usersmodel
    $row = json_decode($papersmodel->parts);
?>

<?php 
    $data = file_get_contents("php://input");
    $data = explode("&", $data);
    array_pop($data);
    for ($i=0; $i < count($data); $i++){
        $data[$i] = explode("=", $data[$i]);
    }
    //echo '<pre />';
    //print_r($data);
    
    $sum = 0.0;
    $answertmp = [];
    $papertmp = [];
    $student_anstmp = [];
    $right_anstmp = [];
    $mysqlqid_tmp = [];
    $oriscore_tmp = [];
    
    // 測驗答案
    for ($i=0; $i < count($data); $i++){
        
        $qtype = substr($data[$i][1], 0, 1);
        $qpart_array = explode("-",$data[$i][0]);
        $qpart = $qpart_array[0] -1;
        $qques = $qpart_array[1] -1;
        $qans = substr($data[$i][1], 1);
        if (isset($qpart_array[2])){
            $subqques = $qpart_array[2] -1;
        }
        if (!isset($row[$qpart][0]->questions[$qques]->ans)){   // for normal type
            $qtype = "E";
        }
        
    switch($qtype){
        case 'A':   // choice type
            $answertmp[$qpart][$qques][0] = "A".$qans;
        break;
        case 'B':   // fill type
            if(isset($subqques)){
                $answertmp[$qpart][$qques][$subqques] = "B".$qans;
            }else{
                $answertmp[$qpart][$qques][0] = "B".$qans;
            }
        break;
        case 'C':   // reading type
            $answertmp[$qpart][$qques][$subqques] = "C".$qans;
        break;
        case 'D':   // mutlchoice type
            if (isset($row[$qpart][0]->questions[$qques]->ans)){
                if (!isset($answertmp[$qpart][$qques])){
                    $answertmp[$qpart][$qques][0] = "D".$qans;
                }else{
                    $answertmp[$qpart][$qques][0] = $answertmp[$qpart][$qques][0].$qans;
                }
            }
        break;
        case 'E':   // normal type
        break;
        }

    }
    //echo '<pre />';
    //print_r($answertmp);
    
    // 正確答案 and 原始分數
    $oris = 0;
    for ($k=0; $k < count($row); $k++){
        for ($q=0; $q < count($row[$k][0]->questions); $q++){
            if (isset($row[$k][0]->questions[$q]->ans)){
                if (isset($row[$k][0]->questions[$q]->sub_topic)){  // reading type
                    for ($a=0; $a < count($row[$k][0]->questions[$q]->ans); $a++){
                            $papertmp[$k][$q][$a] = $row[$k][0]->questions[$q]->ans[$a];
                            $oriscore_tmp[$oris][] = $row[$k][0]->questions[$q]->score;
                    }
                }elseif(!isset($row[$k][0]->questions[$q]->opt_ans)){   // fill type
                    for ($a=0; $a < count($row[$k][0]->questions[$q]->ans); $a++){
                        $papertmp[$k][$q][$a] = $row[$k][0]->questions[$q]->ans[$a];
                        $oriscore_tmp[$oris][] = $row[$k][0]->questions[$q]->score;
                    }
                }else{
                    for ($a=0; $a < count($row[$k][0]->questions[$q]->ans); $a++){
                        if (!isset($papertmp[$k][$q])){
                            $papertmp[$k][$q][0] = $row[$k][0]->questions[$q]->ans[$a];    // choice type
                        }else{
                            $papertmp[$k][$q][0] = $papertmp[$k][$q][0].$row[$k][0]->questions[$q]->ans[$a];  // mutlchoice type
                        }
                    }
                    $oriscore_tmp[$oris] = $row[$k][0]->questions[$q]->score;
                }
            }else{
                $papertmp[$k][$q][0] = "";  // normal type
                $oriscore_tmp[$oris] = $row[$k][0]->questions[$q]->score;
            }
            $oris++;
        }
    }
    //echo '<pre />';
    //print_r($papertmp);
    
    // 比對作答跟正確答案
    for ($i=0; $i < count($papertmp); $i++){
        for ($j=0; $j < count($papertmp[$i]); $j++){
            if (isset($answertmp[$i][$j])){
                for ($a=0; $a < count($papertmp[$i][$j]); $a++){
                    if (isset($answertmp[$i][$j][$a])){
                        if (!strcasecmp(substr($answertmp[$i][$j][$a], 1), $papertmp[$i][$j][$a])){
                            $sum = $sum + $row[$i][0]->questions[0]->score;
                        }
                    }
                }

            }
        }
    }
?>

<div class="row">
    <div class="large-8 columns">
        <h2><?php echo $papersmodel->name; ?></h2>
    </div>
    <div class="large-4 columns">
        <span style="font-size: 160%">得分 : </span>
        <span style="color:red;font-size: 560%"><fontPW> <?php echo $sum ?> </fontPW></span>
        <span style="font-size: 160%">分</span><br /><br />
    </div>
</div>

<div class="row">
    <div class="large-12 columns">
        <!--
        <dl class="accordion" data-accordion>
            <dd>
                <a href="#panel2"><h5>作答狀況</h5></a>
                <div id="panel2" class="content">
        -->
<?php
    echo "考試描述: " .$papersmodel->desc. "<br /><br />";
    echo "考卷有效時間: " .Yii::app()->format->formatDate($papersmodel->start_at). "---" .Yii::app()->format->formatDate($papersmodel->end_at). "<br /><br />";
?>
<?php
    for ($k=0; $k < count($row); $k++){
        $kpart_num = $k + 1;
        echo "<br /><span class='alert round label'>Part $kpart_num </span><br /><br />";
        for ($q=0; $q < count($row[$k][0]->questions); $q++){
            if (isset($row[$k][0]->questions[$q]->sub_topic)){
                for ($a=0; $a < count($row[$k][0]->questions[$q]->ans); $a++){
                    $colorbtn = Yii::app()->getModule('oxexam')->color_for_reading($answertmp, $papertmp, $k, $q, $a);
                    echo "<a href='#' class='button tiny radius' style='margin-left:6px;background-color:#$colorbtn;' data-dropdown='drop$k-$q-$a'>";
                    echo ($q+1)."-".($a+1);
                    echo "</a>";
                    echo "<div id='drop$k-$q-$a' data-dropdown-content class='f-dropdown conten'>";
                    Yii::app()->getModule('oxexam')->ans_for_reading($answertmp, $papertmp, $k, $q, $a);
                    echo "</div>";
                }
            }
            else{
                $colorbtn = Yii::app()->getModule('oxexam')->color_for($answertmp, $papertmp, $k, $q);
                echo "<a href='#' class='button tiny radius' style='margin-left:6px;background-color:#$colorbtn;' data-dropdown='drop$k-$q'>";
                $qnum = $q + 1;
                if ($qnum < 100){
                $qnum = "0".$qnum;
                    if ($qnum < 10){ $qnum = "0".$qnum; }
                }
                echo $qnum;
                echo "</a>";
                echo "<div id='drop$k-$q' data-dropdown-content class='f-dropdown conten'>";
                if (isset($answertmp[$k][$q])){
                    Yii::app()->getModule('oxexam')->ans_for($answertmp, $papertmp, $k, $q);
                }else{
                    Yii::app()->getModule('oxexam')->nonans_for($papertmp, $k, $q);
                }
                echo "</div>";
            }
        }
    }
?>
            <!--</div>
            </dd>
        </dl> -->
    </div>
</div>

<?php
    $student_anstmp = Yii::app()->getModule('oxexam')->studentanswer($student_anstmp, $answertmp, $papertmp);
    $right_anstmp = Yii::app()->getModule('oxexam')->rightanswer($right_anstmp, $papertmp);
    $mysqlqid_tmp = Yii::app()->getModule('oxexam')->mysqlqid($mysqlqid_tmp, $row);
    //~ echo '<pre />';
    //print_r($student_anstmp);
    //~ print_r($right_anstmp);
    //~ print_r($mysqlqid_tmp);
    //~ print_r($oriscore_tmp);
?>




<?php
    $record = [];
    for ($i=0; $i < count($right_anstmp); $i++){
        $record[] = 
            [
                "mysql_q_id" => $mysqlqid_tmp[$i],
                "student_ans" => $student_anstmp[$i],
                "right_ans" => $right_anstmp[$i],
                "ori_score" => $oriscore_tmp[$i],
            ];
    }
    //~ print_r($record);
    //~ echo json_encode($record);
    $result = [
        'mysql_exam_id' => $papersmodel->mysql_exam_id,
        'mysql_paper_id' => $papersmodel->mysql_paper_id,
        'mysql_u_id' => $usersmodel->mysql_u_id,
        'name' => $usersmodel->name,
        'start_at' => $papersmodel->start_at,
        'end_at' => $papersmodel->end_at,
        'score' => $sum,
        'record' => $record,
    ];
    //~ echo '<pre />';
    //~ print_r($result);
?>

<?php

    $jresult = json_encode($result, JSON_UNESCAPED_UNICODE);
    $checkresult = Results::model()->findByAttributes(array('mysql_u_id' => $user->mysql_u_id, 'mysql_exam_id' => $papersmodel->mysql_exam_id));
    if (isset($checkresult)){
        //~ $model4update=$this->ResultsloadModel($user->mysql_u_id);
        //~ $model = $model4update;
        $model = $checkresult;
    }else{
        $model=new Results;
    }
    $jrecord = json_encode($result['record']);
    
    $model->mysql_exam_id = $result['mysql_exam_id'];
    $model->mysql_paper_id = $result['mysql_paper_id'];
    $model->mysql_u_id = $result['mysql_u_id'];  
    $model->name = $result['name'];  
    $model->start_at = $result['start_at'];
    $model->end_at = $result['end_at'];
    $model->score = $result['score'];
    $model->record = $jrecord;

    $model->save();
    
    
    // remove mysql_exam_id from exam_info
    $usersexaminfo = json_decode($usersmodel->exam_info);
    $checkexam_info = array_search($papersmodel->mysql_exam_id, $usersexaminfo);
    if (strlen($checkexam_info)){
        unset($usersexaminfo[$checkexam_info]);
        $usersexaminfo = array_values($usersexaminfo);
        $usersexaminfo = json_encode($usersexaminfo);
        $usersmodel->exam_info = $usersexaminfo;
    }
    
    // add mysql_exam_id to exam_end
    $usersexam_end = json_decode($usersmodel->exam_end);
    if (empty($usersexam_end)) { $usersexam_end = []; }
    $checkexam_end = array_search($papersmodel->mysql_exam_id, $usersexam_end);
    if (!strlen($checkexam_end)){
        $usersexam_end[] = $papersmodel->mysql_exam_id;
        $usersexam_end = json_encode($usersexam_end);
        $usersmodel->exam_end = $usersexam_end;
    }
    
    $usersmodel->save();

    //echo '<pre />';
    //print_r($result);

    Yii::import('ext.httpclient.*');
    $client = new EHttpClient("http://".Yii::app()->getModule('oxexam')->loadconf('resultaddress')."/OxQuestion/modules/oxquestion/upload_exam_paper.php", array(
        'maxredirects' => 0,
        'timeout'      => 30));
    $response = $client->setRawData("results=$jresult")->request('POST');
?>

<br />
<div class="row">
    <div class="large-8 columns">
<a class="button" href="test/test_list">返回考試列表</a>
<a class="button" href="#">觀看詳解</a>
    </div>
</div>

<div ng-app="myApp" ng-controller="UserCtrl">

    <div class="row">
        <div class="large-12 columns">
        <alert ng-repeat="alert in alerts" type="alert.type" close="alert.close()">
            <span data-ng-bind-html="alert.msg"></span>
        </alert>
        </div>
    </div>

    <div class="row" >
        <div class="large-12 columns">
            <?php
                if (isset($response)){
                    if($response->isSuccessful() && strpos(json_decode($response->getBody())->msg, "完成")){
                        echo $msg = json_decode($response->getBody())->msg;
                        //echo "<div ng-controller=\"salertCtrl\" ng-init=\"alertboxs('$msg')\"></div>";
                        //echo '<pre>' . htmlentities($response->getBody()) .'</pre>';
                    }else{
                        echo $msg = json_decode($response->getRawBody())->msg;
                        //echo "<div ng-controller=\"walertCtrl\" ng-init=\"alertboxs('$msg')\"></div>";
                        //echo $response->getRawBody();
                    }
                }
            ?>
        </div>
    </div>

</div>


</div>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/foundation/foundation.dropdown.js"></script>
<script>
    $(document).foundation();
</script>
</body>
</html>

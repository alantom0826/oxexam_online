<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/vendor/jquery.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/foundation/foundation.js"></script>
    <script src="http://cdn.foundation5.zurb.com/foundation.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/oxexam.js"></script>
    <script>
        $(document).ready(function(){
            $('#ad').trigger('click');
        });
    </script>
    <script type="text/javascript">

</script>
    <style type="text/css">
	ul, li {
		margin: 0;
		padding: 0;
		list-style: none;
	}
	.abgne_tab {
        float: left;
		/*clear: left;*/
		width: 240px;
		margin: 10px 0;
        position: relative;
        
	}
	ul.tabs {
		width: 100%;
		height: 30px;
		border-bottom: 1px solid #999;
		border-left: 1px solid #999;
	}
	ul.tabs li {
		/*float: left;*/
		height: 30px;
		line-height: 30px;
		/*overflow: hidden;*/
		position: relative;
		/*margin-bottom: -1px;*/	/* 讓 li 往下移來遮住 ul 的部份 border-bottom */
		border: 1px solid #999;
		/*border-left: none;*/
		background: #e1e1e1;
	}
	ul.tabs li a {
		display: block;
		padding: 0 20px;
		color: #000;
		border: 1px solid #fff;
		text-decoration: none;
	}
	ul.tabs li a:hover {
		background: #ccc;
	}
	ul.tabs li.active  {
		background: #fff;
		border-bottom: 1px solid #fff;
	}
	ul.tabs li.active a:hover {
		background: #fff;
	}
	div.tab_container {
		/*clear: left;*/
		width: 70%;
        height: 300px;
		border: 1px solid #999;
        position: relative;
        left: 25%;
        top: -30%;
		/*border-top: none;*/
		background: #fff;
	}
	div.tab_container .tab_content {
		padding: 20px;
	}
	div.tab_container .tab_content h2 {
		margin: 0 0 20px;
	}
</style>
</head>
<?php
    $cookie_adname = "adEnable".$usersmodel->username;
    if (!isset($_COOKIE[$cookie_adname])){
        setcookie($cookie_adname, 1, time()+2592000);
    }else{
        Yii::app()->getModule('oxexam')->adEnable = $_COOKIE[$cookie_adname];
    }
?>

<?php
/* php測試區 */
    //echo "<input type='button' value='清除 cookie' onclick=\"DeleteCookie('".$cookie_adname."');\">";
    //echo "<input type='button' value='Show Cookie' onclick='ShowCookie();'>";
?>

<body>
<?php Yii::app()->getModule('oxexam')->isTesting = false; ?>

<?php if (Yii::app()->getModule('oxexam')->adEnable): ?>

<a hidden data-reveal-id="myad" id="ad" >ad</a>

<div id="myad" class="reveal-modal" data-reveal>
  <h2>歡迎蒞臨 高明一對一數位學習中心</h2>
  <p class="lead">診斷式題庫使用流程</p>
<div class="abgne_tab">
		<ul class="tabs">
			<li><a href="#tab1">登入>首頁</a></li>
			<li><a href="#tab2">選擇目前試卷庫</a></li>
            <li><a href="#tab3">挑選適合您考試範圍之試卷</a></li>
			<li><a href="#tab4">一次性作答交卷(不可暫停)</a></li>
            <li><a href="#tab5">批改及閱讀詳解</a></li>
			<li><a href="#tab6">輸入手機驗證碼</a></li>
            <li><a href="#tab7">閱讀診斷分析報告</a></li>
		</ul>
<br /><br /><br /><br /><br /><br />

	</div>

		<div class="tab_container">
			<div id="tab1" class="tab_content">
                <h3>tab1</h3>
				<h2>關於作者</h2>
				<p>目前工作是網頁開發為主，因此針對了 HTML, JavaScript, CSS 等知識特別深入研究。若有任何問題，歡迎直接留言或是透過 Mail 討論。</p>
			</div>
			<div id="tab2" class="tab_content">
                <h3>tab2</h3>
				<h2>jQuery is a new kind of JavaScript Library.</h2>
				<p>jQuery is a fast and concise JavaScript Library that simplifies HTML document traversing, event handling, animating, and Ajax interactions for rapid web development. jQuery is designed to change the way that you write JavaScript</p>
			</div>
            <div id="tab3" class="tab_content">
                <h3>tab3</h3>
				<h2>關於作者</h2>
				<p>目前工作是網頁開發為主，因此針對了 HTML, JavaScript, CSS 等知識特別深入研究。若有任何問題，歡迎直接留言或是透過 Mail 討論。</p>
			</div>
			<div id="tab4" class="tab_content">
                <h3>tab4</h3>
				<h2>jQuery is a new kind of JavaScript Library.</h2>
				<p>jQuery is a fast and concise JavaScript Library that simplifies HTML document traversing, event handling, animating, and Ajax interactions for rapid web development. jQuery is designed to change the way that you write JavaScript</p>
			</div>
            <div id="tab5" class="tab_content">
                <h3>tab5</h3>
				<h2>關於作者</h2>
				<p>目前工作是網頁開發為主，因此針對了 HTML, JavaScript, CSS 等知識特別深入研究。若有任何問題，歡迎直接留言或是透過 Mail 討論。</p>
			</div>
			<div id="tab6" class="tab_content">
                <h3>tab6</h3>
				<h2>jQuery is a new kind of JavaScript Library.</h2>
				<p>jQuery is a fast and concise JavaScript Library that simplifies HTML document traversing, event handling, animating, and Ajax interactions for rapid web development. jQuery is designed to change the way that you write JavaScript</p>
			</div>
            <div id="tab7" class="tab_content">
                <h3>tab7</h3>
				<h2>關於作者</h2>
				<p>目前工作是網頁開發為主，因此針對了 HTML, JavaScript, CSS 等知識特別深入研究。若有任何問題，歡迎直接留言或是透過 Mail 討論。</p>
			</div>
		</div>
<br /><br />      
  <input class="button radius" value="開始考試">
  <input class="button radius" value="觀看說明影片">
  <div>
  <input type="checkbox" name="adenable" onclick="createCookie('<?php echo $cookie_adname; ?>',0,30);"/>下次不顯示
  </div>
  <a class="close-reveal-modal">&#215;</a>        
</div>
<?php endif; ?>
<?php 
    // $user = Yii::app()->getModule('user')->user(Yii::app()->user->id);   // modify for render usersmodel
    Yii::import('application.controllers.TestController');
    
    // check test expire
    if (isset($usersmodel)){
        $row = json_decode($usersmodel->exam_info);
        for ($i=0; $i < count($row); $i++){
            $papersmodel = $this->PaperloadModel($row[$i]);
            if (isset($papersmodel)){
                if (strtotime($papersmodel->end_at) < time()){
                    $papersmodel->expire = 1;
                }else{
                    if (strtotime($papersmodel->start_at) > time()){
                        $papersmodel->expire = 2;
                    }else{
                        $papersmodel->expire = 0;
                    }
                }
                $papersmodel->save();
            }
        }
    }
?>
    
<ul class="breadcrumbs" data-tab>
    <li class="tab-title active"><a href="#panel2-1"><?php echo UserModule::t('Execute'); ?></a></li>
    <li class="tab-title"><a href="#panel2-2"><?php echo UserModule::t('Finished'); ?></a></li>
    <li class="tab-title"><a href="#panel2-3"><?php echo UserModule::t('Prepare'); ?></a></li>
</ul>
<div class="tabs-content">
    <div class="content active" id="panel2-1">
    <div class="panel">目前可以考試的考卷</div>
        <div class="container" id="page">
        <br />
        <?php
            // if(!empty($user)){   // modify for render usersmodel
                // $usersmodel=$this->UsersloadModel($user->mysql_u_id);    // modify for render usersmodel
                if (isset($usersmodel)){
                    $row = json_decode($usersmodel->exam_info);
                    $key = rand();
                    $rootpath=Yii::app()->getModule('oxexam')->loadconf('rootpath');//到oxquestion路徑
                    //echo "<div id=\"main\" class=\"row\">";
                    echo "<div id=\"main\">";
                    for ($i=0; $i < count($row); $i++){
                        $papersmodel = $this->PaperloadModel($row[$i]);
                        
                        if (isset($papersmodel)){
                            if (!$papersmodel->expire){
                                $cryptnum = base64_encode($row[$i] + $key);
                                
                                //** use reveal
                                echo "<div id=\"myModal$i\" class=\"reveal-modal\" data-reveal>";
                                echo "<h4>$papersmodel->name</h4><br />";
                                echo "考卷有效時間: " .Yii::app()->format->formatDate($papersmodel->start_at). "---" .Yii::app()->format->formatDate($papersmodel->end_at). "<br /><br />";
                                echo "測驗時間: " .$papersmodel->duration. "分<br /><br />";
                                echo "考試描述: " .$papersmodel->desc. "<br /><br />";
                                //~ echo "<a class=\"button\" href=\"/oxexam_online/test/testing?mysql_exam_id=$cryptnum&K$key\">開始考試</a><br />";
                                $Start_Exam =  UserModule::t('Start Exam');
                                echo "<a class=\"button radius\" href=\"/oxexam_online/test/testing?mysql_exam_id=$cryptnum&K$key\">$Start_Exam</a><br />";
                                echo "<a class=\"close-reveal-modal\">×</a>";
                                echo "</div>";

                                $testpath = "/oxexam_online/img/quiz.png";//圖片測試路徑
				echo "<div class=\"small-3 large3 columns\">";
                                //~ echo "<img style='width:200px;height:75%;'src='$rootpath$testpath'>" ;
                                echo "<ul class='pricing-table'>";                                
				echo "<li class='price'><img src='$testpath' href='#' data-reveal-id='myModal$i' data-reveal></li>" ;
				echo "<li class='description'>$papersmodel->name</li>";
                                $Click_Papers = UserModule::t('Click Papers');
                                echo "<li class='cta-button'><a class='button expand radius' href='#' data-reveal-id='myModal$i' data-reveal>$Click_Papers</a></li>";
				echo "</ul>";
                                //echo "<a class='button expand radius' href='#' style='margin-top:10px; width:200px;' data-reveal-id='myModal$i' data-reveal>$Click_Papers</a>";
                                //echo "<p style='width:200px;'>$papersmodel->name</p>";
                                echo "</div>";
                            }
                        }
                    }
                     echo "</div>";
                }else{
                    // do nothing
                }
            // }    // modify for render usersmodel
        ?>
        </div>
    </div>


    <div class="content" id="panel2-2">
    <div class="panel">已過期或完成作答的考卷</div>
        <div class="container" id="page">
        <br />
        <?php
            if (isset($usersmodel)){
                $row = json_decode($usersmodel->exam_info);
                $rootpath=Yii::app()->getModule('oxexam')->loadconf('rootpath');//到oxquestion路徑
                echo "<div id=\"main\">";
                $usersexam_end = json_decode($usersmodel->exam_end);
                if (empty($usersexam_end)) { $usersexam_end = []; }
                for ($i=0; $i < count($usersexam_end); $i++){
                    $papersmodel = $this->PaperloadModel($usersexam_end[$i]);
                    if (isset($papersmodel)){
                        //** use reveal
                        echo "<div id=\"mydModal$i\" class=\"reveal-modal\" data-reveal>";
                        echo "<h4>$papersmodel->name</h4><br />";
                        echo "考卷有效時間: " .$papersmodel->start_at. "---" .$papersmodel->end_at. "<br /><br />";
                        echo "測驗時間: " .$papersmodel->duration. "分<br /><br />";
                        echo "考試描述: " .$papersmodel->desc. "<br /><br />";
                        echo "<a class=\"close-reveal-modal\">×</a>";
                        echo "</div>";
                        $testpath = "/oxexam_online/img/quiz.png";//圖片測試路徑
                        echo "<div class=\"small-3 large3 columns\">";
                        echo "<ul class='pricing-table'>";                                
                        echo "<li class='price'><img src='$testpath' href='#' data-reveal-id='mydModal$i' data-reveal></li>";
			echo "<li class='description'>$papersmodel->name</li>";
                        $Exam_Finish = UserModule::t('Exam Finish');
                        echo "<li class='cta-button'><a class='button expand radius' href='#' style='background-color:#269926;' data-reveal-id='mydModal$i' data-reveal>$Exam_Finish</a></li>";

                        //echo "<p style='width:200px;'>$papersmodel->name</p>";
                        echo "</div>";
                    }
                }
                for ($i=0; $i < count($row); $i++){
                    $papersmodel = $this->PaperloadModel($row[$i]);
                    if (isset($papersmodel)){
                        if ($papersmodel->expire == "1"){
                            //** use reveal
                            echo "<div id=\"mydModal$i\" class=\"reveal-modal\" data-reveal>";
                            echo "<h4>$papersmodel->name</h4><br />";
                            echo "考卷有效時間: " .$papersmodel->start_at. "---" .$papersmodel->end_at. "<br /><br />";
                            echo "測驗時間: " .$papersmodel->duration. "分<br /><br />";
                            echo "考試描述: " .$papersmodel->desc. "<br /><br />";
                            echo "<a class=\"close-reveal-modal\">×</a>";
                            echo "</div>";
                            $testpath = "/oxexam_online/img/quiz.png";//圖片測試路徑
                            echo "<div class=\"small-3 large3 columns\">";
                            echo "<img style='width:200px;height:200px;'src='$testpath'>" ;
                            $Exam_Timeout= UserModule::t('Exam Timeout');
                            echo "<a class='button expand radius' href='#' style='margin-top:10px; width:200px;background-color:#5d6163;' data-reveal-id='mydModal$i' data-reveal>$Exam_Timeout</a>";
                            echo "<p style='width:200px;'>$papersmodel->name</p>";
                            echo "</div>";
                        }
                    }
                }
                echo "</div>";

            }else{
                // do nothing
            }
        ?>
        </div>
    </div>

    <div class="content" id="panel2-3">
    <div class="panel">準備中考試的考卷</div>
        <div class="container" id="page">
        <br />
            <?php
                if (isset($usersmodel)){
                $row = json_decode($usersmodel->exam_info);
                $rootpath=Yii::app()->getModule('oxexam')->loadconf('rootpath');//到oxquestion路徑
                echo "<div id=\"main\">";
                for ($i=0; $i < count($row); $i++){
                    $papersmodel = $this->PaperloadModel($row[$i]);
                    if (isset($papersmodel)){
                        if ($papersmodel->expire == "2"){
                            //** use reveal
                            echo "<div id=\"mydModal$i\" class=\"reveal-modal\" data-reveal>";
                            echo "<h4>$papersmodel->name</h4><br />";
                            echo "考卷有效時間: " .$papersmodel->start_at. "---" .$papersmodel->end_at. "<br /><br />";
                            echo "測驗時間: " .$papersmodel->duration. "分<br /><br />";
                            echo "考試描述: " .$papersmodel->desc. "<br /><br />";
                            echo "<a class=\"close-reveal-modal\">×</a>";
                            echo "</div>";
                            $testpath = "/oxexam_online/img/quiz.png";//圖片測試路徑
                            echo "<div class=\"small-3 large3 columns\">";
                            echo "<ul class='pricing-table'>";                                
                            echo "<li class='price'><img src='$testpath' href='#' data-reveal-id='mydModal$i' data-reveal></li>";
			    echo "<li class='description'>$papersmodel->name</li>";
                            $Exam_Timeout= UserModule::t('Exam Prepare');
                            echo "<li class='cta-button'><a class='button expand radius' href='#' style='background-color:#f06d32;' data-reveal-id='mydModal$i' data-reveal>$Exam_Timeout</a>";
                            //echo "<p style='width:200px;'>$papersmodel->name</p>";
                            echo "</div>";
                        }
                    }
                }
                echo "</div>";

            }else{
                // do nothing
            }
            ?>
        </div>
    </div>
</div>

<script>
    $(document).foundation();
</script>

</body>
</html>

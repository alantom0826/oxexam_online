<script type="text/x-mathjax-config">
MathJax.Hub.Config({
    HTML: ["input/TeX","output/HTML-CSS"],
    TeX: { extensions: ["AMSmath.js","AMSsymbols.js"],
            equationNumbers: { autoNumber: "AMS" } },
    extensions: ["tex2jax.js"],
    jax: ["input/TeX","output/HTML-CSS"],
    tex2jax: { inlineMath: [ ['`','`'], ["\\(","\\)"] ],
                processEscapes: true },
    "HTML-CSS": { availableFonts: ["TeX"],
                    linebreaks: { automatic: true } }
});
</script>

<!-- MathJax 另外再打包成一個套件 -->
<!--
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/MathJax/MathJax.js?config=TeX-AMS_HTML-full"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/MathJax/MathJax.js?config=TeX-AMS-MML_SVG"></script>
-->

<!-- MathJax JS -->
<script type="text/javascript" src="http://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS_HTML-full"></script>
<script type="text/javascript" src="http://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_SVG"></script>

<style id="test_math">
input[type = "text"],
textarea {
    display: inline;
}
</style>

<script>
$(function() {
    // for fill type
    $("#esubmit").click(function(){
        $("input[type^='text']").each(function (indexs) {
            $(this).val(function(n,content){
                return "B" + content;
            });
        });
        $("input[type^='text']").attr('type', 'password');
    });
});
</script>

<?php 
//~ require_once('db_conn.php');
?>
<br /><br /><br />
<!--
<div ng-app='TimeoutModal' ng-controller="TimeCtrl" ng-init="start(<?php //echo $papersmodel->duration; ?>,0)">
-->
<div ng-init="start(<?php echo $papersmodel->duration; ?>,0)">

    <alert ng-repeat="alert in alerts" type="alert.type" close="alert.close()">
        <span data-ng-bind-html="alert.msg"></span>
    </alert>
<div class="container">
    <modal-dialog ng-click="start()" show='modalShown'>
    暫停<br>
    按任何鍵返回考試
    </modal-dialog>
    
<!-- 試卷標題 -->
<div class="row">
<!--
    <div class="large-12 large-offset-3 large-centered columns"><h1><?php echo $papersmodel->name;?></h1></div>
-->
    <div class="large-12 large-centered columns"><h2>【<?php echo $papersmodel->name;?>】</h2></div>
</div>

<div class="row">
    <div class="large-12 columns">
<!--
        <div class='large-12 columns fixed' style='width:120px; left:85%; top:8.5%;' ><input type="image" src="<?php //echo Yii::app()->request->baseUrl; ?>/img/pause-sign.png" "height="35" width="35" ng-click="pause()"></div>
-->
<!--
        <div class='large-12 columns fixed' style='width:120px; left:88%; top:8%;' ><h3 class="timefont" ><span data-ng-bind-html="hours"></span><span>:</span><span data-ng-bind-html="minutes"></span><span>:</span><span data-ng-bind-html="seconds"></span></h3></div>
		<button id="screen_button" class='tiny fixed radius' style='width:120px; left:88%; top:14%;' >確認答題數</button>
-->
        <form name="eform" method="post" action="/oxexam_online/result">
<?php
    //add by kevin 判斷是否有paper的描述資訊
    if (strlen($papersmodel->desc)>0) {
	echo "<div class='panel'>" . $papersmodel->desc . "</div>";
    }

    //$rootpath="/OxQuestion/modules/oxquestion/";
    $rootpath=Yii::app()->getModule('oxexam')->loadconf('rootpath');
    $extpath= Yii::app()->request->baseUrl."/ext";
    
    // k => part 數量
    // q => question 數量
    // a => answer 數量
    for ($k=0; $k < count($row); $k++){
        $part_num=$k+1;
	echo "<div class='alert-box info radius'><h4>第" . ($k+1) . "大題：" . $row[$k][0]->name. "</h4></div>";
        //echo "<br /><br />Part" .($k+1) .$row[$k][0]->name. "<br />";
        if (isset($row[$k][0]->questions)){
            for ($q=0; $q < count($row[$k][0]->questions); $q++){//撈出questions 資料使用
                
                if(!isset($row[$k][0]->questions[$q]->ans) && !isset($row[$k][0]->questions[$q]->opt_ans)){
                    $q_num = $q + 1;
                    echo "<p id='topic_$part_num-$q_num'>"." "."</p>";
                    //echo "計算題";
                    //echo "<br />";
                    if (!empty($row[$k][0]->questions[$q]->topic)){
                        $topic = $row[$k][0]->questions[$q]->topic[0];
                        echo "<div class='alert-box topic radius'><h5>". $q_num . ". " .$topic. "</h5></div>";
                    }
                    echo "<div class='alert-box topic radius'><h5>". $q_num . "預設題目</h5></div>";
                    echo "<input type='text' maxlength='10' size='35' style=' width:13.5%; font-size:20px;' name='$part_num-$q_num' id='qq' value=''/>";
                }
                elseif(!isset($row[$k][0]->questions[$q]->opt_ans) && !isset($row[$k][0]->questions[$q]->opt_ans_desc) && isset($row[$k][0]->questions[$q]->ans)){
                    $q_num = $q + 1;
                    echo "<p id='topic_$part_num-$q_num'>"." "."</p>";
                    //echo "填充題";
                    //echo "<br />";
                    if (!empty($row[$k][0]->questions[$q]->topic)){
                        if (isset($row[$k][0]->questions[$q]->pics)){
                            $pics = $row[$k][0]->questions[$q]->pics[0];
                            $pics = "/".$row[$k][0]->questions[$q]->mysql_q_id ."/". $pics;
                            $filepath = $rootpath. "capture/img".$pics;
                            $topic=str_replace("{{pic}}","<p><p><img style='width:50%;height:50%; 'src='$filepath'></p>",$row[$k][0]->questions[$q]->topic);
                            for ($a=0; $a < count($row[$k][0]->questions[$q]->ans); $a++){
                                $aa = $a+1;
                                $topic=substr_replace($topic,"{{q-$aa}}",strpos($topic,"{{q}}"),5);
                            }
                            for ($a=0; $a < count($row[$k][0]->questions[$q]->ans); $a++){
                                $aa = $a+1;
                                $topic=str_replace("{{q-$aa}}","<input type='text' maxlength='10' size='35' style=' width:13.5%; font-size:20px;' name='$part_num-$q_num-$aa' id='qq' value=''/>",$topic);
                            }
                            echo "<div class='alert-box topic radius'><h5>". $q_num . ". " .$topic. "</h5></div>";
                        }else{
                            $topic = $row[$k][0]->questions[$q]->topic;
                            for ($a=0; $a < count($row[$k][0]->questions[$q]->ans); $a++){
                                $aa = $a+1;
                                $topic=substr_replace($topic,"{{q-$aa}}",strpos($topic,"{{q}}"),5);
                            }
                            for ($a=0; $a < count($row[$k][0]->questions[$q]->ans); $a++){
                                $aa = $a+1;
                                $topic=str_replace("{{q-$aa}}","<input type='text' maxlength='10' size='35' style=' width:13.5%; font-size:20px;' name='$part_num-$q_num-$aa' id='qq' value=''/>",$topic);
                            }
                            echo "<div class='alert-box topic radius'><h5>". $q_num . ". " .$topic. "</h5></div>";
                        }
                    }
                }
                elseif(isset($row[$k][0]->questions[$q]->opt_ans_desc) && isset($row[$k][0]->questions[$q]->opt_ans) && !isset($row[$k][0]->questions[$q]->opt_ans_pics)){
                    $check_choose = count($row[$k][0]->questions[$q]->ans);
                    $q_num = $q + 1;
                    echo "<p id='topic_$part_num-$q_num'>"." "."</p>";
                    //echo "圖文選文+題文選文";
                    //echo "<br />";
                    if (strpos($row[$k][0]->questions[$q]->topic, "{{pic}}")){
                        $pics = $row[$k][0]->questions[$q]->pics[0];
                        $pics = "/".$row[$k][0]->questions[$q]->mysql_q_id ."/". $pics;
                        $filepath = $rootpath. "capture/img".$pics;
                        $topic=str_replace("{{pic}}","<p><p><img style='width:50%;height:50%; 'src='$filepath'></p>",$row[$k][0]->questions[$q]->topic);
                        echo "<div class='alert-box topic radius'><h5>". $q_num . ". " .$topic. "</h5></div>";//題目敘述 + 圖片
                    }else{
                        echo "<div class='alert-box topic radius'><h5>". $q_num . ". " .$row[$k][0]->questions[$q]->topic. "</h5></div>";
                    }
                    for ($i=0; $i < count($row[$k][0]->questions[$q]->opt_ans); $i++){ 
                        $opt_ans = $row[$k][0]->questions[$q]->opt_ans[$i];
                        if($check_choose>1){
                            echo "<input style=\"WIDTH: 15px; HEIGHT: 15px\" type=\"checkbox\" name=\"$part_num-$q_num\" value=D$opt_ans>";
                        }else{
                            echo "<input style=\"WIDTH: 15px; HEIGHT: 15px\" type=\"radio\" name=\"$part_num-$q_num\" value=A$opt_ans>";
                        }
                        echo $row[$k][0]->questions[$q]->opt_ans[$i];
                        echo ". ";
                        echo "<label for=\"$q_num\" style=\"display: inline;\">" .$row[$k][0]->questions[$q]->opt_ans_desc[$i]. "</label><br />";
                    }

                }
                elseif(isset($row[$k][0]->questions[$q]->opt_ans_desc) && isset($row[$k][0]->questions[$q]->opt_ans) && isset($row[$k][0]->questions[$q]->opt_ans_pics)){
                    $check_choose= count($row[$k][0]->questions[$q]->ans);
                    $q_num = $q + 1;
                    echo "<p id='topic_$part_num-$q_num'>"." "."</p>";
                    //echo "題圖選圖+題文選圖";
                    //echo "<br />";
                    if (strpos($row[$k][0]->questions[$q]->topic, "{{pic}}")){
                        $pics = $row[$k][0]->questions[$q]->pics[0];
                        $pics = "/".$row[$k][0]->questions[$q]->mysql_q_id ."/". $pics;
                        $filepath = $rootpath. "capture/img".$pics;
                        $topic=str_replace("{{pic}}","<img style='width:50%;height:50%;' src='$filepath'><br/>",$row[$k][0]->questions[$q]->topic);
                        echo "<div class='alert-box topic radius'><h5>". $q_num . ". " .$topic. "</h5></div>";//題目敘述 + 圖片
                    }else{
                        echo "<div class='alert-box topic radius'><h5>". $q_num . ". " .$row[$k][0]->questions[$q]->topic. "</h5></div>";
                    }
                    for ($i=0; $i < count($row[$k][0]->questions[$q]->opt_ans); $i++){
                        $opt_ans = $row[$k][0]->questions[0]->opt_ans[$i];
                        if($check_choose>1){
                            echo "<input style=\"WIDTH: 15px; HEIGHT: 15px; display:inline\" type=\"checkbox\" name=\"$part_num-$q_num\" value=D$opt_ans>";
                        }else{
                            echo "<input style=\"WIDTH: 15px; HEIGHT: 15px; display:inline\" type=\"radio\" name=\"$part_num-$q_num\" value=A$opt_ans>";
                        }
                        echo $row[$k][0]->questions[$q]->opt_ans[$i];
                        echo ". ";
                        $filepath = $rootpath. "capture/img/" .$row[$k][0]->questions[$q]->mysql_q_id. "/";
                        $img_path= $filepath.$row[$k][0]->questions[$q]->opt_ans_pics[$i];
                        echo "<label style=\"display:inline;\" for=\"$q_num\">"."<img class='img_inline' id='img_'.\"$part_num-$q_num\" src='$img_path'>"."</label>";
                    }
                }
            }
        }
    }

?>
            <br/>
            <input type="hidden" name="mysql_exam_id" value="<?php echo $mysql_exam_id ?>">
            <input id="esubmit" class="button radius" type="submit" value="作答結束" ng-click="disreloadbox()">
            <input class="button radius" type="reset" value="全部清除">
        </form>

<div id='screen' class='large-12 columns fixed' style='background:rgba(10%, 10%, 10%, 0.75);top:80%;height:40%;display: none;'>
<div id='scroll' class='large-12 columns' style='margin-left:3%;background:rgba(10%, 10%, 10%, 0.0);top:0%;height:40%;width:95%'>

<!-- 題目按鈕-->
<?php
    for ($k=0; $k < count($row); $k++){
        
        $row_path=$row[$k][0];
        $part_num=$k+1;
		echo "<br /><span class='alert round label'style='top:-5%;'>Part $part_num </span><br />";
        for ($q=0; $q < count($row_path->questions); $q++){
            $q_num = $q + 1 ;
            if(isset($row_path->questions[$q]->sub_topic)){
                 //echo "<p id='all_questions' style='display: none;'>$q_num</p>";
                 //echo "<p id='all_questions_sub' style='display: none;'>$q_num_sub</p>";
                
                for($j=0;$j<count($row_path->questions[$q]->sub_topic); $j++){
                    $q_num_sub = $j+1;
                    echo "<a id='btn_$part_num-$q_num-$q_num_sub'class='button tiny radius' style='margin-buttom:50px;margin-right:3px;' onclick=\"topicpos('$part_num-$q_num-$q_num_sub')\">" .$q_num.'-'.$q_num_sub."</a>";
                }
            }else{
                //echo "<p id='all_questions' style='display: inline;'>$q_num</p>";
                if ($q_num < 100){
                    $q_nums = "0".$q_num;
                    if ($q_num < 10){ $q_nums = "0".$q_nums; }
                    echo "<a id='btn_$part_num-$q_num' class='button tiny radius' style='margin-buttom:50px;margin-right:3px;' onclick=\"topicpos('$part_num-$q_num')\">" .$q_nums."</a>";
                }else{
                    echo "<a id='btn_$part_num-$q_num' class='button tiny radius' style='margin-buttom:50px;margin-right:3px;' onclick=\"topicpos('$part_num-$q_num')\">" .$q_num."</a>";
                }
            }
            
            if(isset($row_path->questions[$q]->sub_topic)){//題目的子題數(有的話才放)
                echo "<p id='all_questions_sub' style='display: none;'>$q_num_sub</p>";
            }
        }
        echo "<p id='all_questions' style='display: none;'>$q_num</p>";//題目數                
    }echo "<p id='all_parts' style='display: none;'>$part_num</p>";//PART數 
?>
        </div>
		</div>

    </div>
</div>
</div>

<?php 
//~ require_once('db_close.php');
?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>

<style id="testing-css">
body > iframe { display: none; }

input[type=radio] + label {
    color: black;
    font-style: normal;
} 
input[type=radio]:checked + label {
    font-style: italic;
    font-weight:bold;
    color: #f06d32;
}

input[type=checkbox] + label {
    color: black;
    font-style: normal;
} 
input[type=checkbox]:checked + label {
    font-style: italic;
    font-weight:bold;
    color: #f06d32;
}

.timefont { 
  color: #f06d32;  
}

.img_inline{
	width:15%;
	height:15%;
	display:inline;
	}

body {
    overflow-x:hidden;
}
</style>

    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/vendor/jquery.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.slimscroll.min.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/angular.min-1.2.9.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/angular-sanitize-1.2.0-rc.2.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/mm-foundation-tpls-0.2.1.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/timeout.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/foundation.min.js"></script>

    
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/timeout.css" />

<script type="text/javascript" language="javascript">
/*------判断radio是否有选中，获取选中的值--------*/
$(function() {
    $("#screen_button").click(function() {
		$("#screen").toggle();
    });
    
     /* $("#screen").click(function() { 
		 $("#screen").hide();
     });*/
    
    $('#scroll').slimScroll({
    color: '#f06d32',
    size: '10px',
    height: '150px',
    alwaysVisible: true
    });

	$("input:enabled").click(function(){/* radio點選後同步改變對應的button*/
		var loop_parts = $('#all_parts').text();
		var loop_questions = $('#all_questions').text();
		var loop_questions_sub = $('#all_questions_sub').text();
		loop_parts = parseInt(loop_parts);
		loop_questions = parseInt(loop_questions);
		loop_questions_sub = parseInt(loop_questions_sub);
		for(var p=0;p<loop_parts;p++){//被選擇的按鈕會變暗
			var p_num=p+1;
			for(var q=0;q<loop_questions;q++){
				var q_num=q+1;
				var check_radio = $("input[name="+p_num+"-"+q_num+"]:checked").size();
				var input_text = $("#text_"+p_num+"-"+q_num).val();//填充取值
				if(input_text){//填充題
					$('#btn_'+p_num+"-"+q_num).css('background','rgba(10%, 10%, 10%, 0.1)');
				}
				
				if(check_radio != 0){
					$('#btn_'+p_num+"-"+q_num).css('background','rgba(10%, 10%, 10%, 0.1)');
                }
				if(loop_questions_sub){//有子提
					for(var s=0;s<loop_questions_sub;s++){
						var s_num=s+1;
						var check_radio = $("input[name="+p_num+"-"+q_num+"-"+s_num+"]:checked").size(); 
						var test=p_num+"-"+q_num+"-"+s_num;
						if(check_radio != 0){
							$('#btn_'+p_num+"-"+q_num+"-"+s_num).css('background','rgba(10%, 10%, 10%, 0.1)');
                        }
                    }
                }
            }
        }
	});
    
    $("input:reset").click(function(){
        var loop_parts = $('#all_parts').text();
		var loop_questions = $('#all_questions').text();
		var loop_questions_sub = $('#all_questions_sub').text();
		loop_parts = parseInt(loop_parts);
		loop_questions = parseInt(loop_questions);
		loop_questions_sub = parseInt(loop_questions_sub);
		for(var p=0;p<loop_parts;p++){
			var p_num=p+1;
			for(var q=0;q<loop_questions;q++){
                    var q_num=q+1;
                    $('#btn_'+p_num+"-"+q_num).css('background','rgb(28.2%,51%,67.5%)');
                if(loop_questions_sub){
                    for(var s=0;s<loop_questions_sub;s++){
                        var s_num=s+1;
                        $('#btn_'+p_num+"-"+q_num+"-"+s_num).css('background','rgb(28.2%,51%,67.5%)');
                    }
                }
            }
        }
    });

    var docHeight = $(document).height();
    $(".ng-modal-overlay").css({height:docHeight + 1500});
    
});

function topicpos(q_num){
    var t = $('#topic_'+q_num).offset().top;
    $(window).scrollTop(t-45);
}

</script>

</head>
<body>
<?php Yii::app()->getModule('oxexam')->isTesting = true; ?>
    
<a id="toTop" class="fixed" href="#" style="display: inline; width:120px; left:90%; top:90%;">
<img width="40" height="40" alt="To Top" src="/oxexam_online/img/to-top@2x.png">
</a>

<?php
    $key = substr(strrchr ($_SERVER['QUERY_STRING'], "&K"), 2);
    $mysql_exam_id=$_GET["mysql_exam_id"];
    $mysql_exam_id = base64_decode($mysql_exam_id)-$key;
    //Yii::import('application.controllers.TestController');    // modify for render usersmodel
    $papersmodel=$this->PaperloadModel($mysql_exam_id);
    $papersmodel->parts =str_replace("amp;", "", $papersmodel->parts);
    $row = json_decode($papersmodel->parts);//取出table->parts 的data
?>

<?php
//~ $testmath=$_GET["testmath"];
$ptype = $papersmodel->subject_id;
switch($ptype){
    case '1': 
        require_once('test_eng.php');
    break;
    case '3':
    require_once('test_math.php');
    break;
}
?>

</body>
</html>

<?php /* @var $this Controller */ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>

	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="en" />

	<!-- blueprint CSS framework -->
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/screen.css" media="screen, projection" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/print.css" media="print" />

	<!--[if lt IE 8]>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css" media="screen, projection" />
	<![endif]-->

	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css" />
    
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/foundation5.2.x.min.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/oxexam.css" />

    
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/vendor/modernizr.js"></script>

	<title><?php echo CHtml::encode($this->pageTitle); ?></title>


<style type="text/css">
    top-bar-title a {color: #ffffff;font-size: 120%;}

    @font-face{
        font-family : PWChalk;
        src : url("<?php echo Yii::app()->request->baseUrl; ?>/fonts/PWChalk.ttf");
    }
    fontPW { font-family : PWChalk;}

    @font-face{
        font-family : NSHThin;
        src : url("<?php echo Yii::app()->request->baseUrl; ?>/fonts/NotoSansHantThin.otf") format("opentype");
    }
    fontThin { font-family : NSHThin;}

    @font-face{
        font-family : wt021;
        src : url("<?php echo Yii::app()->request->baseUrl; ?>/fonts/wt021.ttf");
    }
    fontwt021 { font-family : wt021;}

    body {
        font-family: NSHThin;
    }
</style>
</head>

<body>
<div ng-app='TimeoutModal' ng-controller="TimeCtrl">
<div class="fixed">
    <nav class="top-bar" data-topbar>
        <ul class="title-area">
            <li class="name">
                <!--<h1><a href="#">OxExam_Online</a></h1> -->
                <h1><a href="#"><?php echo Yii::app()->getModule('oxexam')->loadconf('projectname'); ?></a></h1>
            </li>
            <!-- Remove the class "menu-icon" to get rid of menu icon. Take out "Menu" to just have icon alone -->
            <li class="toggle-topbar menu-icon"><a href="#"><span>Menu</span></a></li>
        </ul>
        
        <section class="top-bar-section">
            <!-- Right Nav Section -->
            <ul class="right">
                <?php $this->widget('zii.widgets.CMenu',array(
                    'encodeLabel' => false,
                    'items'=>array(
                        array('url'=>'#', 'label'=>'預約試聽', 'visible'=>Yii::app()->user->isGuest),
                        array('url'=>Yii::app()->getModule('user')->loginUrl, 'label'=>Yii::app()->getModule('user')->t("Login"), 'visible'=>Yii::app()->user->isGuest),
                        //array('url'=>Yii::app()->getModule('user')->registrationUrl, 'label'=>Yii::app()->getModule('user')->t("Register"), 'visible'=>Yii::app()->user->isGuest),
                        array('url'=>'#', 'label'=>'<span class="timefont" data-ng-bind-html="hours"></span><span class="timefont">:</span><span class="timefont" data-ng-bind-html="minutes"></span><span class="timefont">:</span><span class="timefont" data-ng-bind-html="seconds"></span>', 'visible'=>!Yii::app()->user->isGuest && Yii::app()->getModule('oxexam')->isTesting),
                        array('url'=>'#', 'label'=>'<a id="screen_button">確認答題數</a>', 'visible'=>!Yii::app()->user->isGuest && Yii::app()->getModule('oxexam')->isTesting),
                        array('url'=>array('/test/test_list'), 'label'=>UserModule::t('Tests'), 'visible'=>!Yii::app()->user->isGuest),
                        array('url'=>array('/setup/setup'), 'label'=>UserModule::t('Setup'), 'visible'=>!Yii::app()->user->isGuest && UserModule::isAdmin()),
                        array('url'=>Yii::app()->getModule('user')->profileUrl, 'label'=>Yii::app()->getModule('user')->t("Profile"), 'visible'=>!Yii::app()->user->isGuest && UserModule::isAdmin()),
                        array('url'=>Yii::app()->getModule('user')->logoutUrl, 'label'=>Yii::app()->getModule('user')->t("Logout").' ('.Yii::app()->user->name.')', 'visible'=>!Yii::app()->user->isGuest),
                        
                    ),
                )); ?>
                <?php if(Yii::app()->user->isGuest): ?>
                <script src="http://cdn.foundation5.zurb.com/foundation.js"></script>
                <li class="has-dropdown">
                    <a href="#"><?php echo UserModule::t("Language"); ?></a>
                    <ul class="dropdown">
                <?php
                    foreach (Yii::app()->UrlManager->listLanguage() as $language => $languageUrl) {
                        echo '<ul>';
                        if (Yii::app()->language==$language) {
                            echo '<li>'.CHtml::label(Yii::app()->getModule('oxexam')->lang($language), $language, array('style'=>'color:#f06d32;font-size:100%;')).'</li>';
                        }else{
                            echo '<li>'.CHtml::link(Yii::app()->getModule('oxexam')->lang($language),$languageUrl).'</li>';
                        }
                        echo '</ul>';
                    }
                ?>
                    </ul>
                </li>
                <?php endif; ?>
            </ul>
            <!-- Left Nav Section -->
            <ul class="left">
                <li><a href="#"><?php echo "Ver".Yii::app()->getModule('oxexam')->loadconf('version'); ?></a></li>
            </ul>
        </section>
    </nav>
</div>

	<?php if(isset($this->breadcrumbs)):?>
		<?php $this->widget('zii.widgets.CBreadcrumbs', array(
			'links'=>$this->breadcrumbs,
		)); ?><!-- breadcrumbs -->
	<?php endif?>

	<?php echo $content; ?>

	<div class="clear"></div><!-- clear -->

	<div id="footer">
		Copyright &copy; <?php echo date('Y'); ?> by OSS Integral Institute Co., Ltd.<br/>
		All Rights Reserved.<br/>
		<?php echo Yii::powered(); ?>
	</div><!-- footer -->

</div><!-- ng-app='TimeoutModal' -->

<!--
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/vendor/fastclick.js"></script>
-->
<script> $(document).foundation(); </script>
</body>
</html>

<?php

/**
 * This is the model class for table "users".
 *
 * The followings are the available columns in table 'users':
 * @property string $exam_info
 * @property string $exam_end
 * @property string $name
 * @property integer $mysql_u_id
 * @property string $class_info
 * @property string $email
 * @property integer $id
 * @property string $username
 * @property string $password
 * @property string $activkey
 * @property string $create_at
 * @property string $lastvisit_at
 * @property integer $superuser
 * @property integer $status
 */
class Users extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'users';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			//array('exam_info, exam_end, name, mysql_u_id, class_info, email, id, username, password, activkey, create_at, superuser, status', 'required'),
            array('username, name, mysql_u_id, class_info, password', 'required'),
			array('mysql_u_id, id, superuser, status', 'numerical', 'integerOnly'=>true),
			array('name, email', 'length', 'max'=>60),
			array('username', 'length', 'max'=>20),
			array('password, activkey', 'length', 'max'=>128),
			array('lastvisit_at', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('exam_info, exam_end, name, mysql_u_id, class_info, email, id, username, password, activkey, create_at, lastvisit_at, superuser, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'exam_info' => 'Exam Info',
			'exam_end' => 'Exam End',
			'name' => 'Name',
			'mysql_u_id' => 'Mysql U',
			'class_info' => 'Class Info',
			'email' => 'Email',
			'id' => 'ID',
			'username' => 'Username',
			'password' => 'Password',
			'activkey' => 'Activkey',
			'create_at' => 'Create At',
			'lastvisit_at' => 'Lastvisit At',
			'superuser' => 'Superuser',
			'status' => 'Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('exam_info',$this->exam_info,true);
		$criteria->compare('exam_end',$this->exam_end,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('mysql_u_id',$this->mysql_u_id);
		$criteria->compare('class_info',$this->class_info,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('id',$this->id);
		$criteria->compare('username',$this->username,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('activkey',$this->activkey,true);
		$criteria->compare('create_at',$this->create_at,true);
		$criteria->compare('lastvisit_at',$this->lastvisit_at,true);
		$criteria->compare('superuser',$this->superuser);
		$criteria->compare('status',$this->status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Users the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}

<?php

/**
 * This is the model class for table "results".
 *
 * The followings are the available columns in table 'results':
 * @property integer $mysql_exam_id
 * @property integer $mysql_paper_id
 * @property integer $mysql_u_id
 * @property string $name
 * @property string $start_at
 * @property string $end_at
 * @property double $score
 * @property string $choice
 * @property string $reading
 */
class Results extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'results';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			//~ array('mysql_exam_id, mysql_paper_id, mysql_u_id, name, start_at, score, choice, reading', 'required'),
            array('mysql_exam_id, mysql_paper_id, mysql_u_id, name, start_at, score, choice', 'required'),
			array('mysql_exam_id, mysql_paper_id, mysql_u_id', 'numerical', 'integerOnly'=>true),
			array('score', 'numerical'),
			array('name', 'length', 'max'=>255),
			array('end_at', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('mysql_exam_id, mysql_paper_id, mysql_u_id, name, start_at, end_at, score, choice, reading', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'mysql_exam_id' => 'Mysql Exam',
			'mysql_paper_id' => 'Mysql Paper',
			'mysql_u_id' => 'Mysql U',
			'name' => 'Name',
			'start_at' => 'Start At',
			'end_at' => 'End At',
			'score' => 'Score',
			'choice' => 'Choice',
			'reading' => 'Reading',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('mysql_exam_id',$this->mysql_exam_id);
		$criteria->compare('mysql_paper_id',$this->mysql_paper_id);
		$criteria->compare('mysql_u_id',$this->mysql_u_id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('start_at',$this->start_at,true);
		$criteria->compare('end_at',$this->end_at,true);
		$criteria->compare('score',$this->score);
		$criteria->compare('choice',$this->choice,true);
		$criteria->compare('reading',$this->reading,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Results the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}

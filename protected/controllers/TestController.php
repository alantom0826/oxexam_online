<?php
class TestController extends Controller {
    
    public function actionTesting(){
        
        //$papersmodel=new Paper;
        //$this->render('testing',array('papersmodel'=>$papersmodel));
        
        $this->render('testing');
    }
    
    public function actionTest_List(){
        
        $user = Yii::app()->getModule('user')->user(Yii::app()->user->id);
        if (!empty($user)){
            $usersmodel=$this->UsersloadModel($user->mysql_u_id);
            //$this->render('test_list');
            $this->render('test_list',array('usersmodel'=>$usersmodel));
        }else{
            $this->redirect('/oxexam_online/user/login');
        }
    }
    
    //~ public function actionTest01(){
        //~ $this->render('test01');
    //~ }
    
    /**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Users the loaded model
	 * @throws CHttpException
	 */
	public function UsersloadModel($id)
	{
		$model=Users::model()->findByPk($id);
		//~ if($model===null)
			//~ throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}
    
    public function PaperloadModel($id)
	{
		$model=Paper::model()->findByPk($id);
		//~ if($model===null)
			//~ throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}
        
}

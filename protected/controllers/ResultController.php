<?php

class ResultController extends Controller
{
	public function actionIndex()
	{
        $user = Yii::app()->getModule('user')->user(Yii::app()->user->id);
        $usersmodel=$this->UsersloadModel($user->mysql_u_id);
        
        $this->render('index',array('usersmodel'=>$usersmodel));
	}

    /**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Users the loaded model
	 * @throws CHttpException
	 */
	public function UsersloadModel($id)
	{
		$model=Users::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}
    
    public function PaperloadModel($id)
	{
		$model=Paper::model()->findByPk($id);
		//~ if($model===null)
			//~ throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}
    
    public function ResultsloadModel($id)
	{
		$model=Results::model()->findByPk($id);
		//~ if($model===null)
			//~ throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}
    
	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}
